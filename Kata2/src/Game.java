public class Game {

    public String winner(String[] deckSteve, String[] deckJosh) {
        int scoreSteve = 0;
        int scoreJosh = 0;

        for (int i = 0; i < deckSteve.length; i++) {
            int rankSteve = getCardRank(deckSteve[i]);
            int rankJosh = getCardRank(deckJosh[i]);

            if (rankSteve > rankJosh) {
                scoreSteve++;
            } else if (rankSteve < rankJosh) {
                scoreJosh++;
            }
        }

        if (scoreSteve > scoreJosh) {
            return "Steve wins " + scoreSteve + " to " + scoreJosh;
        } else if (scoreSteve < scoreJosh) {
            return "Josh wins " + scoreJosh + " to " + scoreSteve;
        } else {
            return "Tie";
        }
    }

    private int getCardRank(String card) {
        String ranks = "23456789TJQKA";
        return ranks.indexOf(card.charAt(0));
    }

}